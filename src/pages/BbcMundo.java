package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseBbcMundo;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class BbcMundo extends TestBaseBbcMundo {
	
	final WebDriver driver;
	public BbcMundo(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInBbcMundo(String apuntaA,String ambiente) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Bbc Mundo - landing");
	
		String URL = driver.getCurrentUrl();		
			WebElement menuSection1 = driver.findElement(By.xpath("//a[contains(text(), 'Videos Premium')]"));
			 menuSection1.click();
				String elemento1 = driver.findElement(By.xpath("//*[@id=\"checkin\"]/h1")).getText();
					Assert.assertEquals(elemento1,"VIDEOS PREMIUM");
				System.out.println(elemento1+" Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection2 = driver.findElement(By.xpath("//a[contains(text(), 'Noticias')]"));
			 menuSection2.click();
				//String elemento2 = driver.findElement(By.xpath("//*[@id=\"checkin\"]/h1")).getText();
					Assert.assertEquals(URL,"http://bbcmundo.opratel.com/");
				System.out.println("Seccion Noticias "+"Visualizacion Exitosa");
			espera(500);
		
			WebElement menuSection3 = driver.findElement(By.xpath("//a[contains(text(), 'America Latina')]"));
				menuSection3.click();
				    String URL2 = driver.getCurrentUrl();
					Assert.assertEquals(URL2,"http://bbcmundo.opratel.com/?cat=mundo-america-latina");
				System.out.println("Seccion America Latina  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection4 = driver.findElement(By.xpath("//a[contains(text(), 'Internacional')]"));
				menuSection4.click();
					String URL3 = driver.getCurrentUrl();
					Assert.assertEquals(URL3,"http://bbcmundo.opratel.com/?cat=mundo-internacional");
				System.out.println("Seccion Internacional  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection5 = driver.findElement(By.xpath("//a[contains(text(), 'Economía')]"));
				menuSection5.click();
					String URL4 = driver.getCurrentUrl();
					Assert.assertEquals(URL4,"http://bbcmundo.opratel.com/?cat=mundo-economia");
				System.out.println("Seccion Economía  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection6 = driver.findElement(By.xpath("//a[contains(text(), 'Tecnología')]"));
				menuSection6.click();
					String URL5 = driver.getCurrentUrl();
					Assert.assertEquals(URL5,"http://bbcmundo.opratel.com/?cat=mundo-tecnologia");
				System.out.println("Seccion Tecnología  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection7 = driver.findElement(By.xpath("//a[contains(text(), 'Ciencia')]"));
				menuSection7.click();
					String URL6 = driver.getCurrentUrl();
					Assert.assertEquals(URL6,"http://bbcmundo.opratel.com/?cat=mundo-ciencia");
				System.out.println("Seccion Ciencia  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection8 = driver.findElement(By.xpath("//a[contains(text(), 'Salud')]"));
				menuSection8.click();
					String URL7 = driver.getCurrentUrl();
					Assert.assertEquals(URL7,"http://bbcmundo.opratel.com/?cat=mundo-salud");
				System.out.println("Seccion Salud  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection9 = driver.findElement(By.xpath("//a[contains(text(), 'Cultura')]"));
				menuSection9.click();
				String URL8 = driver.getCurrentUrl();
					Assert.assertEquals(URL8,"http://bbcmundo.opratel.com/?cat=mundo-cultura");
				System.out.println("Seccion Cultura  "+"Visualizacion Exitosa");
			espera(500);
			
			WebElement menuSection0 = driver.findElement(By.xpath("//a[contains(text(), 'Deportes')]"));
				menuSection0.click();
					String URL9 = driver.getCurrentUrl();
					Assert.assertEquals(URL9,"http://bbcmundo.opratel.com/?cat=mundo-deportes");
				System.out.println("Seccion Deportes  "+"Visualizacion Exitosa");
			espera(500);

		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Bbc Mundo"); 
		System.out.println();
		System.out.println("Fin de Test Patitas Bbc Mundo - Landing");
	}			
}  

